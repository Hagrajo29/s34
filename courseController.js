const User = require("../models/user.js")
const Course = require("../models/course.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")

module.exports.addCourse = (reqBody) =>{
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})
	return newCourse.save().then((saved, error) =>{
		if (error){
			console.log(error)
			return false
		}else{
			return true
		}
	})
}

/*
module.exports.addCourse = (userData,reqBody) =>{
	return User.findId({userData.userId).then((result) => {
		if(userData.isAdmin === true) {
			let newCourse = new Course({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			})
			return newCourse.save().then((course, error) =>{
				if (error){
					console.log(error)
					return false
				}else{
					return "Course creation successful"
				}
			})
		}else{
			return res.send('You are not an Admin.');
		}
	})
}
*/
