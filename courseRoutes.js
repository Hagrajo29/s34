const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const courseController = require("../controllers/courseController.js")


router.post("/courses", ( req, res ) => {
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
})


router.post("/addCourse", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)
	courseController.addCourse(req.body, userData).then(resultFromController => res.send(resultFromController));
})



module.exports = router;